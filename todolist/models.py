from django.db import models

# Create your models here.
class ToDoItem(models.Model):
	# Charfield = string value
	# DateTimeField = DateTime datatype
	task_name = models.CharField(max_length = 50)
	description = models.CharField(max_length = 200)
	status = models.CharField(max_length = 50, default = "Pending")
	date_created = models.DateTimeField("Date Created")